package org.kustom.api.preset

import android.content.Context
import androidx.annotation.Keep
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

@Suppress("unused")
@Keep
class AssetPresetFile @Keep constructor(private val filePath: String) : PresetFile(
    extractNameFromPath(
        filePath
    ), extractExtFromPath(filePath)
) {
    override val path: String
        get() = String.format("file:///android_asset/%s", filePath)

    @Throws(IOException::class)
    override fun getStream(context: Context, file: String): InputStream {
        val zis = ZipInputStream(context.assets.open(filePath))
        var ze: ZipEntry
        while ((zis.nextEntry.also { ze = it }) != null) {
            if (ze.name == file) {
                return zis
            }
        }
        throw FileNotFoundException("File not found: $filePath/$file")
    }
}

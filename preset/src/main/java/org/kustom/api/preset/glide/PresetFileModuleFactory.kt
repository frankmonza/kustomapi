package org.kustom.api.preset.glide

import android.content.Context
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import org.kustom.api.preset.PresetFile
import java.io.InputStream

class PresetFileModuleFactory internal constructor(context: Context) :
    ModelLoaderFactory<PresetFile, InputStream> {
    private val context: Context = context.applicationContext

    override fun build(unused: MultiModelLoaderFactory): ModelLoader<PresetFile, InputStream> {
        return PresetFileModelLoader(context)
    }

    override fun teardown() {
    }
}

package org.kustom.api.preset.glide

import android.annotation.SuppressLint
import android.content.Context
import com.bumptech.glide.annotation.GlideOption
import com.bumptech.glide.load.Option
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoader.LoadData
import com.bumptech.glide.request.RequestOptions
import org.kustom.api.preset.PresetFile
import java.io.InputStream
import java.security.MessageDigest

class PresetFileModelLoader internal constructor(context: Context) :
    ModelLoader<PresetFile, InputStream> {
    private val context: Context = context.applicationContext

    override fun buildLoadData(
        file: PresetFile,
        w: Int,
        h: Int,
        opts: Options
    ): LoadData<InputStream> {
        return LoadData(
            PresetFileKey(file),
            PresetFileDataFetcher(context, file)
                .setLandscape(opts.get(ORIENTATION_LAND)!!)
        )
    }

    override fun handles(file: PresetFile): Boolean {
        return true
    }

    companion object {
        private const val KEY_ORIENTATION_LAND = "org.kustom.glide.load.orientation"

        /**
         * A boolean option that, if set to `true` causes the loader to prefer landscape
         * thumbnails (this applies only if there is actually a landscape thumbnail, not to komps)
         */
        val ORIENTATION_LAND: Option<Boolean> = Option.disk(
            KEY_ORIENTATION_LAND, false
        ) { keyBytes: ByteArray, value: Boolean, digest: MessageDigest ->
            if (value) digest.update(keyBytes)
        }

        @Suppress("unused")
        @SuppressLint("CheckResult")
        @GlideOption
        fun landscape(options: RequestOptions, value: Boolean) {
            options.set(ORIENTATION_LAND, value)
        }
    }
}
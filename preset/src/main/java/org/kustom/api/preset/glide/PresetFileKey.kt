package org.kustom.api.preset.glide

import com.bumptech.glide.load.Key
import org.kustom.api.preset.PresetFile
import java.security.MessageDigest

class PresetFileKey internal constructor(file: PresetFile) : Key {
    private val path = file.path

    override fun hashCode(): Int {
        return path.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other is PresetFileKey && other.path == path
    }

    override fun toString(): String {
        return path
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(path.toByteArray(Key.CHARSET))
    }
}

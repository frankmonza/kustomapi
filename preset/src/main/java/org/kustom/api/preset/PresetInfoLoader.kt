package org.kustom.api.preset

import android.content.Context
import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

@Suppress("unused")
class PresetInfoLoader private constructor(private val file: PresetFile) {
    private val executor: Executor = Executors.newFixedThreadPool(4)
    private val handler = Handler(Looper.getMainLooper())

    fun load(context: Context, callback: Callback) {
        synchronized(presetInfoCache) {
            if (presetInfoCache.containsKey(file.path)) callback.onInfoLoaded(
                presetInfoCache[file.path]
            )
            else executor.execute {
                val file = if (file.isKomponent) "komponent.json" else "preset.json"
                // Try to load preset info
                val info: PresetInfo = try {
                    this.file.getStream(context, file)?.use { stream ->
                        PresetInfo
                            .fromStream(stream)
                            ?.withFallbackTitle(this.file.name)
                            ?: PresetInfo(title = this.file.name)
                    } ?: throw Exception("Failed to load preset info")
                } catch (e: Exception) {
                    e.printStackTrace()
                    PresetInfo(title = this.file.name)
                }
                handler.post {
                    synchronized(presetInfoCache) {
                        presetInfoCache[this.file.path] = info
                        callback.onInfoLoaded(info)
                    }
                }
            }
        }
    }

    interface Callback {
        fun onInfoLoaded(info: PresetInfo?)
    }

    companion object {
        private val presetInfoCache = HashMap<String, PresetInfo>()
        fun create(file: PresetFile): PresetInfoLoader {
            return PresetInfoLoader(file)
        }
    }
}
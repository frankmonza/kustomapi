package org.kustom.api.preset

import android.content.Context
import androidx.annotation.Keep
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream


@Suppress("unused")
class SDPresetFile @Keep constructor(private val file: File) : PresetFile(
    extractNameFromPath(
        file.path
    ), extractExtFromPath(file.path)
) {

    override val path: String
        get() = file.toURI().toASCIIString()

    @Throws(IOException::class)
    override fun getStream(context: Context, file: String): InputStream {
        val zis = ZipInputStream(BufferedInputStream(FileInputStream(this.file)))
        var ze: ZipEntry
        while ((zis.nextEntry.also { ze = it }) != null) {
            if (ze.name == file) {
                return zis
            }
        }
        throw FileNotFoundException("File not found: $path/$file")
    }
}
package org.kustom.api.preset

/**
 * Preset features
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
class PresetFeatures private constructor(private val mIsMutaisMutablele: Boolean) {

    var flags: Int = 0
        private set

    /**
     * Empty Flags Constructor
     */
    constructor() : this(true)

    /**
     * Build from string array
     *
     * @param flags to start building this
     */
    constructor(flags: String?) : this(true) {
        (flags ?: "")
            .toASCII()
            .trim { it <= ' ' }
            .split(" ".toRegex()).dropLastWhile { it.isEmpty() }
            .map { it.uppercase() }
            .forEach { flag ->
                when (flag) {
                    "LOCATION" -> add(FEATURE_LOCATION)
                    "WEATHER" -> add(FEATURE_WEATHER)
                    "FORECAST" -> add(FEATURE_FORECAST)
                    "GYRO" -> add(FEATURE_GYRO)
                    "ANALOG_CLOCK" -> add(FEATURE_ANALOG_CLOCK)
                    "CALENDAR" -> add(FEATURE_CALENDAR)
                    "MUSIC" -> add(FEATURE_MUSIC)
                    "FITNESS" -> add(FEATURE_FITNESS)
                    "TRAFFIC" -> add(FEATURE_TRAFFIC)
                    "DOWNLOAD" -> add(FEATURE_DOWNLOAD)
                    "SIGNAL" -> add(FEATURE_SIGNAL)
                    "NOTIFICATIONS" -> add(FEATURE_NOTIFICATIONS)
                    "SHELL" -> add(FEATURE_SHELL)
                    "UNREAD" -> add(FEATURE_UNREAD)
                    "CALL" -> add(FEATURE_CALL)
                    "AIR_QUALITY" -> add(FEATURE_AIR_QUALITY)
                }
            }
    }


    /**
     * Bitwise check against flag
     *
     * @param flag the flag to check against
     * @return true if found
     */
    fun contains(flag: Int): Boolean {
        return flags != 0 && flag != 0 && (flags and flag) == flag
    }

    /**
     * Add a flag to current BitMask
     *
     * @param flag the flag to add
     * @return this
     */
    fun add(flag: Int): PresetFeatures {
        if (mIsMutaisMutablele) flags = flags or flag
        else throw IllegalStateException("Cannot add flags to an immutable instance")
        return this
    }

    /**
     * Remove a flag from current BitMask
     *
     * @param flag the flag to add
     */
    fun remove(flag: Int) {
        if (mIsMutaisMutablele) flags = flags and flag.inv()
        else throw IllegalStateException("Cannot add flags to an immutable instance")
    }

    /**
     * Add all flags contained in a KFeatureFlags object
     *
     * @param flags the update flags to add
     */
    fun add(flags: PresetFeatures) {
        add(flags.flags)
    }

    /**
     * Clears BitMask
     */
    fun clear() {
        if (mIsMutaisMutablele) flags = 0
        else throw IllegalStateException("Cannot clear flags of an immutable instance")
    }

    fun serialize(): String {
        return toString()
    }

    override fun equals(other: Any?): Boolean {
        return other is PresetFeatures && flags == other.flags
    }

    override fun hashCode(): Int {
        return flags
    }

    override fun toString(): String {
        if (flags == 0) return ""
        val sb = StringBuilder()
        if (contains(FEATURE_LOCATION)) sb.append("LOCATION ")
        if (contains(FEATURE_WEATHER)) sb.append("WEATHER ")
        if (contains(FEATURE_FORECAST)) sb.append("FORECAST ")
        if (contains(FEATURE_GYRO)) sb.append("GYRO ")
        if (contains(FEATURE_ANALOG_CLOCK)) sb.append("ANALOG_CLOCK ")
        if (contains(FEATURE_CALENDAR)) sb.append("CALENDAR ")
        if (contains(FEATURE_MUSIC)) sb.append("MUSIC ")
        if (contains(FEATURE_FITNESS)) sb.append("FITNESS ")
        if (contains(FEATURE_TRAFFIC)) sb.append("TRAFFIC ")
        if (contains(FEATURE_DOWNLOAD)) sb.append("DOWNLOAD ")
        if (contains(FEATURE_SIGNAL)) sb.append("SIGNAL ")
        if (contains(FEATURE_NOTIFICATIONS)) sb.append("NOTIFICATIONS ")
        if (contains(FEATURE_SHELL)) sb.append("SHELL ")
        if (contains(FEATURE_UNREAD)) sb.append("UNREAD ")
        if (contains(FEATURE_CALL)) sb.append("CALL ")
        if (contains(FEATURE_AIR_QUALITY)) sb.append("AIR_QUALITY ")
        return sb.toString().trim { it <= ' ' }
    }

    private fun String.toASCII(): String {
        return if (this.isEmpty()) this
        else {
            val result = StringBuilder()
            for (i in this.indices) {
                val c = this[i].code
                if (c in 32..122) result.append(this[i].uppercaseChar())
            }
            result.toString().trim { it <= ' ' }
        }
    }

    companion object {
        /**
         * Feature flags, each one represents a feature being used in the preset
         */
        const val FEATURE_LOCATION: Int = 2 shl 1
        const val FEATURE_WEATHER: Int = 2 shl 2
        const val FEATURE_FORECAST: Int = 2 shl 3
        const val FEATURE_GYRO: Int = 2 shl 4
        const val FEATURE_ANALOG_CLOCK: Int = 2 shl 5
        const val FEATURE_CALENDAR: Int = 2 shl 6
        const val FEATURE_MUSIC: Int = 2 shl 7
        const val FEATURE_FITNESS: Int = 2 shl 8
        const val FEATURE_TRAFFIC: Int = 2 shl 9
        const val FEATURE_DOWNLOAD: Int = 2 shl 10
        const val FEATURE_SIGNAL: Int = 2 shl 11
        const val FEATURE_NOTIFICATIONS: Int = 2 shl 12
        const val FEATURE_SHELL: Int = 2 shl 13
        const val FEATURE_UNREAD: Int = 2 shl 14
        const val FEATURE_CALL: Int = 2 shl 15
        const val FEATURE_AIR_QUALITY: Int = 2 shl 16

        /**
         * Static empty feature flag object
         */
        val FLAG_FEATURE_NONE: PresetFeatures = PresetFeatures(false)
    }
}
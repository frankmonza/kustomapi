package org.kustom.api.preset.glide

import android.content.Context
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.LibraryGlideModule
import org.kustom.api.preset.PresetFile
import java.io.InputStream

@GlideModule
class PresetFileModule : LibraryGlideModule() {

    override fun registerComponents(ctx: Context, glide: Glide, registry: Registry) {
        Log.i(TAG, "Registering PresetFile module")
        registry.prepend(
            PresetFile::class.java,
            InputStream::class.java, PresetFileModuleFactory(ctx)
        )
    }

    companion object {
        private val TAG: String = PresetFileModule::class.java.simpleName
    }
}

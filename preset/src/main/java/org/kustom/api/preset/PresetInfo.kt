package org.kustom.api.preset

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.Strictness
import com.google.gson.annotations.SerializedName
import com.google.gson.stream.JsonReader
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.reflect.Type
import java.nio.charset.StandardCharsets
import java.util.UUID

@Suppress("unused")
data class PresetInfo @JvmOverloads constructor(
    @SerializedName("title") val title: String,
    @SerializedName("id") val id: String = UUID.randomUUID().toString(),
    @SerializedName("version") val version: Int = MIN_PRESET_VERSION,
    @SerializedName("description") val description: String = "",
    @SerializedName("author") val author: String = "",
    @SerializedName("email") val email: String = "",
    @SerializedName("archive") val archive: String = "",
    @SerializedName("width") val width: Int = 0,
    @SerializedName("height") val height: Int = 0,
    @SerializedName("xscreens") val xScreens: Int = 0,
    @SerializedName("yscreens") val yScreens: Int = 0,
    @SerializedName("release") val release: Int = 0,
    @SerializedName("features") val features: String? = null,
    @SerializedName("locked") val isLocked: Boolean = false,
    @SerializedName("pflags") val flags: Int = 0,
    @SerializedName("hash") val hash: String? = null,
    @SerializedName("ts") val timeStamp: Long = System.currentTimeMillis()
) {

    override fun toString(): String {
        return buildString {
            append(title)
            append("\n\n$description")
            append("\nAuthor: $author")
        }
    }

    fun withNewId(): PresetInfo {
        return copy(id = UUID.randomUUID().toString())
    }

    fun withNewTimeStamp(): PresetInfo {
        return copy(timeStamp = System.currentTimeMillis())
    }

    fun withVersion(version: Int): PresetInfo {
        return copy(version = version)
    }

    fun withFallbackTitle(fallback: String): PresetInfo {
        return copy(title = title.ifEmpty { fallback })
    }

    fun withSize(width: Int, height: Int): PresetInfo {
        return copy(width = width, height = height)
    }

    fun withTitle(title: String): PresetInfo {
        return copy(title = title)
    }

    fun withAuthor(author: String): PresetInfo {
        return copy(author = author)
    }

    fun withEmail(email: String): PresetInfo {
        return copy(email = email)
    }

    fun withDescription(description: String): PresetInfo {
        return copy(description = description)
    }

    fun withArchive(archive: String): PresetInfo {
        return copy(archive = archive)
    }

    companion object {
        private val TAG: String = PresetInfo::class.java.simpleName

        private const val MIN_PRESET_VERSION = 13

        private val gson: Gson = GsonBuilder()
            .setStrictness(Strictness.LENIENT)
            .setPrettyPrinting()
            .serializeSpecialFloatingPointValues()
            .create()

        @JvmStatic
        fun fromStream(stream: InputStream): PresetInfo? {
            try {
                InputStreamReader(stream, StandardCharsets.UTF_8).use { isr ->
                    JsonReader(
                        BufferedReader(isr)
                    ).use { reader ->
                        reader.beginObject()
                        val name = reader.nextName()
                        if (name == "preset_info") return gson.fromJson(
                            reader,
                            PresetInfo::class.java
                        )
                    }
                }
            } catch (e: IOException) {
                Log.w(TAG, "Unable to read preset from input stream", e)
            }
            return null
        }

        @JvmStatic
        fun fromJson(json: String): PresetInfo? {
            return try {
                val jsonObject = gson.fromJson(json, JsonObject::class.java)
                if (!jsonObject.has("id"))
                    jsonObject.addProperty("id", UUID.randomUUID().toString())
                // Ensure description, author, email and archive are not NULLS
                listOf("description", "author", "email", "archive").forEach {
                    if (!jsonObject.has(it) || jsonObject.get(it).isJsonNull)
                        jsonObject.addProperty(it, "")
                }
                gson.fromJson(jsonObject, PresetInfo::class.java)
            } catch (e: Exception) {
                null
            }
        }
    }
}
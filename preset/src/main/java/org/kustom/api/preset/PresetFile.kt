package org.kustom.api.preset

import android.content.Context
import java.io.IOException
import java.io.InputStream

@Suppress("unused")
abstract class PresetFile(val name: String, val ext: String) {

    val isKomponent: Boolean
        get() = "komp".equals(ext, ignoreCase = true)

    abstract val path: String

    @Throws(IOException::class)
    abstract fun getStream(context: Context, file: String): InputStream?

    override fun toString(): String {
        return String.format("%s.%s", name, ext)
    }

    companion object {

        @JvmStatic
        protected fun extractNameFromPath(path: String): String {
            return path.replace(".*/".toRegex(), "")
                .replace("\\..*".toRegex(), "")
        }

        @JvmStatic
        protected fun extractExtFromPath(path: String): String {
            return path.replace("\\.zip".toRegex(), "")
                .replace(".*\\.".toRegex(), "")
        }
    }
}
package org.kustom.api.preset.glide

import android.content.Context
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.data.DataFetcher
import org.kustom.api.preset.PresetFile
import java.io.InputStream

class PresetFileDataFetcher internal constructor(
    private val context: Context,
    private val presetFile: PresetFile
) :
    DataFetcher<InputStream> {
    private var stream: InputStream? = null
    private var landscape = false

    override fun loadData(p: Priority, callback: DataFetcher.DataCallback<in InputStream>) {
        val thumb = if (presetFile.isKomponent) "komponent_thumb.jpg"
        else if (landscape) "preset_thumb_landscape.jpg"
        else "preset_thumb_portrait.jpg"
        try {
            stream = presetFile.getStream(context, thumb) ?: throw Exception("File not found")
            callback.onDataReady(stream)
        } catch (e: Exception) {
            e.printStackTrace()
            callback.onLoadFailed(e)
        }
    }

    override fun cleanup() {
        try {
            stream?.close()
        } catch (ignored: Exception) {
        }
    }

    override fun cancel() {
    }

    override fun getDataClass(): Class<InputStream> {
        return InputStream::class.java
    }

    override fun getDataSource(): DataSource {
        return DataSource.LOCAL
    }

    fun setLandscape(landscape: Boolean): PresetFileDataFetcher {
        this.landscape = landscape
        return this
    }
}
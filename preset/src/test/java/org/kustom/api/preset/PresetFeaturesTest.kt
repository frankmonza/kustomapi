package org.kustom.api.preset

import org.junit.Assert.*
import org.junit.Test

class PresetFeaturesTest {

    @Test
    fun testEmptyFeaturesShouldHaveNoFlags() {
        val features = PresetFeatures()
        assertEquals("New instance should have flags == 0", 0, features.flags)
        assertTrue("String representation should be empty", features.toString().isEmpty())
    }

    @Test
    fun testAddSingleFeature() {
        val features = PresetFeatures()
        features.add(PresetFeatures.FEATURE_LOCATION)
        assertTrue("Should contain FEATURE_LOCATION", features.contains(PresetFeatures.FEATURE_LOCATION))
        assertFalse("Should not contain FEATURE_WEATHER", features.contains(PresetFeatures.FEATURE_WEATHER))
    }

    @Test
    fun testAddMultipleFeatures() {
        val features = PresetFeatures()
        features.add(PresetFeatures.FEATURE_LOCATION)
            .add(PresetFeatures.FEATURE_WEATHER)
            .add(PresetFeatures.FEATURE_CALL)

        assertTrue("Should contain FEATURE_LOCATION", features.contains(PresetFeatures.FEATURE_LOCATION))
        assertTrue("Should contain FEATURE_WEATHER", features.contains(PresetFeatures.FEATURE_WEATHER))
        assertTrue("Should contain FEATURE_CALL", features.contains(PresetFeatures.FEATURE_CALL))
        assertFalse("Should not contain FEATURE_GYRO", features.contains(PresetFeatures.FEATURE_GYRO))

        // toString should have them in the expected order
        val strRep = features.toString()
        assertTrue("String representation should contain LOCATION", strRep.contains("LOCATION"))
        assertTrue("String representation should contain WEATHER", strRep.contains("WEATHER"))
        assertTrue("String representation should contain CALL", strRep.contains("CALL"))
    }

    @Test
    fun testRemoveFeature() {
        val features = PresetFeatures()
        features.add(PresetFeatures.FEATURE_LOCATION)
        assertTrue(features.contains(PresetFeatures.FEATURE_LOCATION))

        // Remove it
        features.remove(PresetFeatures.FEATURE_LOCATION)
        assertFalse(features.contains(PresetFeatures.FEATURE_LOCATION))
    }

    @Test
    fun testClearFeatures() {
        val features = PresetFeatures()
        features.add(PresetFeatures.FEATURE_LOCATION)
            .add(PresetFeatures.FEATURE_WEATHER)
        assertNotEquals("Flags should not be 0", 0, features.flags)

        features.clear()
        assertEquals("After clear(), flags should be 0", 0, features.flags)
    }

    @Test
    fun testConstructorWithFlagString() {
        // Passing multiple flags as a space-separated string
        val features = PresetFeatures("location weather forecast")

        assertTrue(features.contains(PresetFeatures.FEATURE_LOCATION))
        assertTrue(features.contains(PresetFeatures.FEATURE_WEATHER))
        assertTrue(features.contains(PresetFeatures.FEATURE_FORECAST))
        assertFalse(features.contains(PresetFeatures.FEATURE_CALL))

        val strRep = features.toString()
        // Should have "LOCATION WEATHER FORECAST" in some form
        assertTrue(strRep.contains("LOCATION"))
        assertTrue(strRep.contains("WEATHER"))
        assertTrue(strRep.contains("FORECAST"))
    }

    @Test
    fun testSerialize() {
        val features = PresetFeatures()
        features.add(PresetFeatures.FEATURE_LOCATION)
        features.add(PresetFeatures.FEATURE_WEATHER)

        val serialized = features.serialize()
        // Should be "LOCATION WEATHER" (order may vary depending on code, but typically alphabetical in toString)
        assertTrue(serialized.contains("LOCATION"))
        assertTrue(serialized.contains("WEATHER"))
    }

    @Test
    fun testEqualityAndHashCode() {
        val f1 = PresetFeatures("LOCATION WEATHER")
        val f2 = PresetFeatures()
        f2.add(PresetFeatures.FEATURE_LOCATION)
        f2.add(PresetFeatures.FEATURE_WEATHER)

        assertEquals("Features should be equal", f1, f2)
        assertEquals("hashCode should match for equal objects", f1.hashCode(), f2.hashCode())
    }

    @Test(expected = IllegalStateException::class)
    fun testImmutabilityThrowsOnAdd() {
        // FLAG_FEATURE_NONE is created with `false`, making it immutable
        PresetFeatures.FLAG_FEATURE_NONE.add(PresetFeatures.FEATURE_CALL)
    }

    @Test(expected = IllegalStateException::class)
    fun testImmutabilityThrowsOnRemove() {
        PresetFeatures.FLAG_FEATURE_NONE.remove(PresetFeatures.FEATURE_CALL)
    }

    @Test(expected = IllegalStateException::class)
    fun testImmutabilityThrowsOnClear() {
        PresetFeatures.FLAG_FEATURE_NONE.clear()
    }
}

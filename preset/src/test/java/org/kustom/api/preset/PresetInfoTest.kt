package org.kustom.api.preset

import org.junit.Assert
import org.junit.Test

class PresetInfoTest {

    @Test
    fun testConstructorProperties() {
        val preset = PresetInfo(
            version = 11,
            title = "TestTitle",
            author = "Author",
            isLocked = true
        )
        Assert.assertEquals(11, preset.version.toLong())
        Assert.assertEquals("TestTitle", preset.title)
        Assert.assertEquals("Author", preset.author)
        Assert.assertTrue(preset.isLocked)
    }

    @Test
    fun testCopy() {
        val original = PresetInfo(
            version = 1,
            title = "Original"
        )
        val copy = original.copy(title = "Changed")
        Assert.assertEquals(1, copy.version.toLong())
        Assert.assertEquals("Changed", copy.title)
        Assert.assertNotEquals(original.title, copy.title)
    }

    @Test
    fun testFromJson() {
        val json = """
            {
                "version": 12,
                "title": "JsonTitle",
                "description": "JsonDescription",
                "locked": false
            }
        """.trimIndent()
        val preset = PresetInfo.fromJson(json)
        Assert.assertEquals(12, preset?.version)
        Assert.assertEquals("JsonTitle", preset?.title)
        Assert.assertEquals("JsonDescription", preset?.description)
        Assert.assertFalse(preset?.isLocked ?: true)
    }

    @Test
    fun testFromJsonWithExtraFields() {
        val json = """
            {
                "version": 12,
                "title": "JsonTitle",              
                "malformed": "test",
                "locked": false
            }
        """.trimIndent()
        val preset = PresetInfo.fromJson(json)
        Assert.assertEquals(12, preset?.version)
        Assert.assertEquals("JsonTitle", preset?.title)
        Assert.assertNotNull(preset?.id)
    }

    @Test
    fun testFromJsonWithNulls() {
        val json = """
            {
                "version": 12,
                "title": "JsonTitle",
                "description": null,
                "locked": false
            }
        """.trimIndent()
        val preset = PresetInfo.fromJson(json)
        Assert.assertEquals(12, preset?.version)
        Assert.assertEquals("JsonTitle", preset?.title)
        Assert.assertEquals(preset?.description, "")
    }
}